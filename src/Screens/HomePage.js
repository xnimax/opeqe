import React from "react";
import { Helmet } from "react-helmet";
import Header from "../Sections/Header";
import HeadBanner from "../Sections/HeadBanner";
import CategorySection from "../Sections/CategorySection";
import Ready from "../Sections/Ready";
import BottomIcon from "../Sections/BottomIcon"
import Footer from "../Sections/Footer";
const HomePage = () => {
  const title = `Opeqe Inc. | Opeqe San Fransisco`;
  return (
    <div className="homePage">
      <Helmet>
        <meta charSet="utf-8" />
        <title>{title}</title>
      </Helmet>
      <Header />
      <HeadBanner />
      <CategorySection />
      <Ready />
      <BottomIcon />
      <Footer />
    </div>
  );
};

export default HomePage;

