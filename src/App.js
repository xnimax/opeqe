import React from "react";

//REDUX
import { Provider } from "react-redux";
import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import store from "./store";

//ROUTER
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

//SCREEN
import Home from "./Screens/HomePage";

//SECTION
import MobileMenu from "./Sections/MobileMenu";

//IMPORT END
let store_def = createStore(store, composeWithDevTools());
function App() {
  return (
    <>
      <Provider store={store_def}>
        <Router>
          <Switch>
            <Route component={Home} path="/" exact></Route>
          </Switch>
          <MobileMenu />
        </Router>
      </Provider>
    </>
  );
}
export default App;
