import React from "react";
import data from "../data.json";
import FilterCategory from "../Elements/FilterCategory";
import styled from "styled-components";

const CategorySection = () => {
  return (
    <div className="categorySection">
      <Wrap>
        <FilterCategory data={data} special />
        <FilterCategory data={data} mealType="Lunch & Dinner" />
        <FilterCategory data={data} cuisineType="Mexican" />
        <FilterCategory data={data} cuisineType="Japanese" />
        <FilterCategory data={data} menuType="Pizza" />
        <FilterCategory data={data} menuType="Sandwich" />
        <FilterCategory data={data} mealType="Breakfast" />
        <FilterCategory data={data} menuType="Salad" />
        <FilterCategory data={data} menuType="Soup" />
      </Wrap>
    </div>
  );
};

export default CategorySection;

const Wrap = styled.div`
  max-width: 91%;
  margin-left: auto;
  margin-right: auto;
`;
