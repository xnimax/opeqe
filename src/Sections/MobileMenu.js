import React from "react";
import LocalDiningIcon from "@material-ui/icons/LocalDining";
import ReceiptIcon from "@material-ui/icons/Receipt";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import TodayIcon from "@material-ui/icons/Today";
import PersonIcon from "@material-ui/icons/Person";
const MobileMenu = () => {
  return (
    <div className="mobileMenu">
      <div className="row">
        <div className="col">
          <button
            className="btnmobile"
            onClick={() => alert("Limited Version")}
          >
            <LocalDiningIcon style={{ fontSize: 30 }} className="mobileIcon" />
          </button>
        </div>
        <div className="col">
          <button
            className="btnmobile"
            onClick={() => alert("Limited Version")}
          >
            <ReceiptIcon style={{ fontSize: 30 }} className="mobileIcon" />
          </button>
        </div>
        <div className="col">
          <button
            className="btnmobile"
            onClick={() => alert("Limited Version")}
          >
            <ShoppingBasketIcon
              style={{ fontSize: 30 }}
              className="mobileIcon"
            />
          </button>
        </div>
        <div className="col">
          <button
            className="btnmobile"
            onClick={() => alert("Limited Version")}
          >
            <TodayIcon style={{ fontSize: 30 }} className="mobileIcon" />
          </button>
        </div>
        <div className="col">
          <button
            className="btnmobile"
            onClick={() => alert("Limited Version")}
          >
            <PersonIcon style={{ fontSize: 30 }} className="mobileIcon" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default MobileMenu;
