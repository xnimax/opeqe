import React from 'react';
import ic from "../assets/images/footerTop.svg"
const BottonIcon = () => {
    return (  
        <div className="btIcon">
            <img src={ic} className="footerTopIcon" alt="delivery" />
            <div className="btBg"></div>
        </div>
    );
}
 
export default BottonIcon;