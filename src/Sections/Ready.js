import React from "react";
import styled from "styled-components";

const Ready = () => {
  return (
    <>
      <div className="py-5 botSec bgWhite">
        <Title>Ready to order?</Title>
        <Sub>Browse our menu for dine-in, delivery or pickup and catering</Sub>
      </div>
    </>
  );
};

export default Ready;

const Title = styled.div`
  font-size: 25px;
  width:100%;
`;
const Sub = styled.div`
  font-size: 18px;
  margin-top: 10px;
  width:100%;

`;
