import React from "react";
import homeHeader from "../assets/images/HomeHeader.jpg";

const HeadBanner = () => {
  return (
    <div className="homeImg">
      <img
        className="imgBanner"
        src={homeHeader}
        alt="pizza"
      />
    </div>
  );
};

export default HeadBanner;

