import React, {  useEffect, useState } from "react";
import Wrap from "../Elements/Wrap";
import Left from "../Elements/Left";
import Right from "../Elements/Right";
import Logo from "../Elements/Logo";
import MenuItem from "../Elements/MenuItem";
import styled from "styled-components";

const Header = () => {
  const [show, setShow] = useState(false);
  useEffect(() => {
    // FUNCTION HANDLER
    const headFunc = () => {
      if (window.pageYOffset > 200) {
        setShow(true);
      } else {
        setShow(false);
      }
    };
    setShow(false);
    window.addEventListener("scroll", headFunc);
    //CLEAN UP
    return () => window.removeEventListener("scroll", headFunc);
  }, []);
  return (
    <>
      <header className={show ? "activeHead" : ""}>
        <div className="headWrap">
          <Head>
            <Wrap>
              <Left>
                <div className="leftHead">
                  <Logo />
                </div>
              </Left>
              <Right>
                <div className="rightHead">
                  <MenuItem />
                </div>
              </Right>
            </Wrap>
          </Head>
        </div>
      </header>
    </>
  );
};

export default Header;
const Head = styled.div`
  height: 53px;
  margin: 15px;
`;
