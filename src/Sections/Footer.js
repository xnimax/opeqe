import React, { Fragment } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
const footerTop = [
  "About",
  "Services",
  "Support",
  "Gallery",
  "Terms",
  "Locations",
];

const footerMenu = [
  {
    title: "Main Menu",
    links: [{ name: "Pickup" }, { name: "Delivery" }],
  },
  {
    title: "Orders",
    links: [{ name: "Upcoming Orders" }, { name: "Recent Orders" }],
  },
  {
    title: "Reservation",
    links: [{ name: "Recent Reservation" }, { name: "Wait To Be Seated" }],
  },
  {
    title: "Profile",
    links: [{ name: "Promos & Credits" }, { name: "Rewards" }],
  },
  {
    title: "Support",
    links: [{ name: "Contact Us" }, { name: "Live Chat" }],
  },
  {
    title: "FREE FOOD",
    links: [{ name: "Get $20.00 credit for your next order." }],
  },
  {
    title: "Special Offers",
    clear: true,
    links: [
      { name: "Happy Hour (Egg Burger with Truffle Mayo)" },
      { name: "Happy Hour (California Chicken Salad)" },
      { name: "Happy Hour (Pad Thai)" },
      { name: "Happy Hour (Quarter Dark and Leg)" },
    ],
  },
];

const footerLastMenu = [
  { title: "©2019 OPEQE INC", link: null },
  { title: "Terms & Conditions", link: "#" },
  { title: "Privacy Policy", link: "#" },
];


const footdesctop = `Delight customers everywhere with a branded custom-built native iOS, native Android and Installable Website Application.`;
const footdescbottom = `Opeqe is reliable, fast and commission free all-in-one ordering solutions for multi-location or single location restaurants.`;
const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <div className="footerTopMenu">
          <ul className="text-right">
            {footerTop.map((item, key) => (
              <li className="mx-3" key={key}>
                {item}
              </li>
            ))}
          </ul>
        </div>

        <div className="footerMenu">
          <div className="row">
            {footerMenu.map((item, key) => (
              <div
                key={key}
                className={`${
                  item.clear ? "col-12" : "col-md-3 col-xs-6"
                } my-4`}
              >
                <FootTitle>{item.title}</FootTitle>
                {item.links.map((link, k) => (
                  <FootLink key={k}>{link.name}</FootLink>
                ))}
              </div>
            ))}
          </div>
        </div>

        <div className="footerDesc mt-5">
          <FootDescTop>{footdesctop}</FootDescTop>
          <FootDescBottom>{footdescbottom}</FootDescBottom>
        </div>

        <div className="footLast">
          <div className="row">
            <div className="col-md-8">
              {footerLastMenu.map((lk, kd) => (
                <Fragment key={kd}>
                  {lk.link && (
                    <span>
                      <Link to={lk.link}>{lk.title}</Link>
                    </span>
                  )}
                  {!lk.link && <span>{lk.title}</span>}
                </Fragment>
              ))}
            </div>
            <div className="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;

const FootLink = styled.div`
  color: #a5a5a5;
  cursor: pointer;
  font-size: 16px;
`;
const FootTitle = styled.div`
  font-size: 20px;
  color: #fff;
`;

const FootDescTop = styled.div`
  color: #d7d7d7;
  font-size: 16px;
`;

const FootDescBottom = styled.div`
  color: #a5a5a5;
  font-size: 16px;
  margin-top: 10px;
`;
