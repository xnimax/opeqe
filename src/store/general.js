const initialState = {
  mobile: false,
  drawer: false,
  login: false,
  remember: false,
  register: false,
  activateUser: false,
  homedata: null,
  lightbox: false,
  searchmode: false,
  loading:false,
  lightbox_images: [],
  maincity: localStorage.getItem("maincity") || 60,
};
function general(state = initialState, action) {
  switch (action.type) {
    case "LOADING":
      return {...state,loading:true}
    case "UNLOADING":
      return {...state,loading:false}
    case "SET_LIGHTBOX_IMAGE":
      var images = action.data;
      return { ...state, lightbox_images: images };
    case "SEARCHMODE_ON":
      return { ...state, searchmode: true };
    case "SEARCHMODE_OFF":
      return { ...state, searchmode: false };
    case "LIGHTBOX_OPEN":
      return { ...state, lightbox: true };
    case "LIGHTBOX_CLOSE":
      return { ...state, lightbox: false };
    case "SET_MAIN_CITY":
      var maincity = action.data;
      return { ...state, maincity };
    case "SET_HOME_DATA":
      var homed = action.data;
      return { ...state, homedata: homed };
    case "TOGGLE_MOBILE":
      const mobile = !state.mobile;
      return { ...state, mobile };
      
    case "DRAWER_CLOSE":
      return { ...state, drawer: false };
    case "TOGGLE_REGISTER":
      const register = !state.register;
      return { ...state, register };
    case "DRAWER_OPEN":
      return { ...state, drawer: true };
    case "TOGGLE_LOGIN":
      const login = !state.login;
      return { ...state, login };
    case "TOGGLE_REMEMBER":
      const remember = !state.remember;
      return { ...state, remember };
    case "ACTIVATE_USER":
      const activateUser = !state.activateUser;
      return { ...state, activateUser };
    default:
      return state;
  }
}

export default general;
