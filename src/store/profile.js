const initialState = {
    data: null
};

function profile(state = initialState, action) {
    switch (action.type) {
        case "SET_PROFILE":
            return { data: action.profile };
        case "LOGOUT":
            return initialState;
        default:
            return state;
    }
}

export default profile;
