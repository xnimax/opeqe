import React from "react";
import { Link } from "react-router-dom";
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
const data = [
  { name: "Reservation", link: "new" },
  { name: "Orders", link: "order-history" },
  { name: "Locations", link: "locations" },
];
const MenuItem = () => {
  return (
    <div className="menuItems">
      <ul className="mItem">
        {data.map((item, key) => (
          <li key={key}>
            <Link to={item.link}>{item.name}</Link>
          </li>
        ))}
        <li>
          <button className="btn btnLogin">Log In</button>
        </li>
        <li>
          <button className="btn btnSignup">Sign Up</button>
        </li>
        <li>
          <button className="btnCart ripple">
              <ShoppingBasketIcon style={{fontSize:"30px"}} />
          </button>
        </li>
      </ul>
    </div>
  );
};

export default MenuItem;
