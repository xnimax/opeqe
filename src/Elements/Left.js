import styled from "styled-components";
const Left = styled.section`
  justify-content: left;
  flex: 1;
`;
export default Left;
