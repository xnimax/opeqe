import styled from "styled-components";
const Wrap = styled.section`
  max-width: 91%;
  margin: 0 auto;
  display:flex;
  justify-content: flex-end;
  flex-direction:row;

`;
export default Wrap;
