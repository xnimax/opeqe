import React from "react";
import styled from "styled-components";
import { Card } from "@material-ui/core";
import TimerIcon from "@material-ui/icons/Timer";
const CategoryItem = ({ data, happy }) => {
  function addPercent(val) {
    return parseInt(val + (val * 50) / 100);
  }
  return (
    <Card>
      <button onClick={() => alert("Limited Version")} className="btnTest">
        <div className="cat">
          <div className="imgContainer">
            <img className="img-fluid" src={data.image} alt="item.title" />
          </div>
          <Title>{data.title}</Title>
          <MType>{data.menuType.title}</MType>
          <Attr>
            {data.cuisineType.title}, {data.courseType.title},{" "}
            {data.mealType.title}
          </Attr>
          <div className="row">
            <div className="col-7 bold">
              <TimerIcon style={{ fontSize: "14px" }} /> {data.preparation} -{" "}
              {addPercent(data.preparation)} Mins <Price>${data.price}</Price>
            </div>
            <div className="col-5">
              <MType right={true}>Free Pickup</MType>
            </div>
          </div>
        </div>
      </button>
    </Card>
  );
};

export default CategoryItem;

const Title = styled.div`
  font-size: 18px;
`;

const MType = styled.span`
  color: #026764;
  font-size: 15px;
  display: block;
  text-align: ${(props) => (props.right ? "right" : "left")};
`;
const Attr = styled.div`
  color: #696969;
`;

const Price = styled.span`
  background: #d7d7d7;
  padding: 0 3px;
`;
