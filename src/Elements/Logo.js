import React from "react";
import logo from "../assets/images/opeqe-logo.svg";
import styled from "styled-components";
const Logo = () => {
  return (
    <Lg>
      <img
        style={{
          width: "170px",
          height: "50px",
          verticalAlign: "middle",
          margin: "0px",
        }}
        className="img-fluid"
        src={logo}
        alt="Opeqe"
      />
    </Lg>
  );
};

export default Logo;

const Lg = styled.div`
  width: 170px;
  height: 50px;
  margin-left:28px;
`;
