import React, { useState, useEffect } from "react";
import CategoryItem from "./CategoryItem";
import styled from "styled-components";
import Carousel from "../Elements/Carousel";

const FilterCategory = ({
  data,
  mealType = null,
  cuisineType = null,
  special = null,
  menuType = null,
}) => {
  const [title, setTitle] = useState(null);
  const [cat, setCat] = useState(null);
  useEffect(() =>{ 
    
    function filterData() {
      if (special) {
        const a = data[0].items.filter(
          (item) =>
            item.special &&
            item.special.title &&
            item.special.quantity !== 0 &&
            item.special.remainingTime > 0
        );
        setCat(a);
        filterTitle(a);
      }
      if (mealType) {
        filterTitle("null");
        setCat(
          data[0].items.filter(
            (item) => item.mealType && item.mealType.title === mealType
          )
        );
      }
      if (cuisineType) {
        filterTitle("null");
        setCat(
          data[0].items.filter(
            (item) => item.cuisineType && item.cuisineType.title === cuisineType
          )
        );
      }
      if (menuType) {
        filterTitle("null");
        setCat(
          data[0].items.filter(
            (item) => item.menuType && item.menuType.title === menuType
          )
        );
      }
    }
    filterData()
  
  }, [menuType,special,cuisineType,mealType,data]);
  
  function filterTitle(dt) {
      if (special) {
        if (dt[0].special && dt[0].special.title) {
          setTitle(dt[0].special.title);
        }
      }
      if (mealType) {
        setTitle(mealType);
      }
      if (cuisineType) {
        setTitle(cuisineType);
      }
      if (menuType) {
        setTitle(menuType);
      }
    }

  
  return (
    <div className="catItem">
      <Title> {cat && title}</Title>
      <Bar>
        <span className="barSpan" style={{ left: 0 }}></span>
      </Bar>
      {cat && (
        <>
          <Carousel splitIndex={0} collapse={false}>
            {cat.map((item, key) => (
              <CategoryItem key={key} data={item} />
            ))}
          </Carousel>
        </>
      )}
    </div>
  );
};

export default FilterCategory;
const Title = styled.div`
  font-size: 20px;
  margin-top: 30px;
  font-weight: 700;
`;
const Bar = styled.div`
  width: 100%;
  height: 1px;
  background-color: #d7d7d7;
  display: block;
  margin: 10px 0 20px;
  position: relative;
`;
